/**   
 * Code class - this class generates the CPU code.
 * 
 * EE422C programming assignment #2  
 * Name: Dedow, Karl and Fernando, Rukshinie
 * UT EID: kfd235 and rf9447
 * Lab Section: 16825 (Wednesday at 10:30)
 * EE422C - Assignment #2
 * @author Karl Dedow and Rukshinie Fernando
 * @version 1.0 2014-10-02
 * */
package Assignment2;

import java.util.Random;

public class Code implements Colors
{
	String code;

	/**
	 * Constructor for the code class. This constructor generates the CPU code by
	 * calling the generateCode() method.
	 * 
	 */
	public Code()
	{
		code = generateCode();
	}

	/**
	 * This method generates the actual CPU code. The method utilizes the Random object to 
	 * generate a number between 0 and NUM_COLORS - 1. This number is then associated with
	 * a specific color in the set of valid colors to get a character representing the
	 * color. From here, a string is slowly built that will eventually be our CPU code.
	 * 
	 * @return temp.toString() - this string represents the CPU generated code
	 */
	public String generateCode()
	{
		System.out.println("Generating secret code...\n");
		StringBuilder temp = new StringBuilder();
		Random rand = new Random();

		// Generate a random number between 0 and NUM_COLORS - 1. Each number
		// will represent a specific color.
		for (int i = 0; i < NUM_SPACES; i++)
		{
			// The number generator
			int num = rand.nextInt(NUM_COLORS);
			
			// Call buildCode(): buildCode() will associate a number with
			// a specific color char in the set of valid colors
			char nextColor = buildCode(num);
			
			// Append the color to the string
			temp.append(nextColor);
		}
		
		// Return the CPU code
		return temp.toString();
	}

	/**
	 * Associates the generated number with a color from our color array. Once the color char
	 * is generated return the value, so that the CPU code strong can continue to be built.
	 * Essentially, this method converts an integer to a color.
	 * 
	 * @param num - a number that represents a specific color. Given the number, return a specific color
	 * 				 associated with that number.
	 * 
	 * @return the color (which is a char type) that the number is associated with
	 */
	private char buildCode(int num)
	{
		// The swtich statement associates a specific number with a specific color
		// Return the color
		switch (num)
		{
		case 0:
			return COLORS[0];
		case 1:
			return COLORS[1];
		case 2:
			return COLORS[2];
		case 3:
			return COLORS[3];
		case 4:
			return COLORS[4];
		case 5:
			return COLORS[5];
		default:
			return 0;
		}
	}
}
