/**   
 * Player class for the mastermind experiment
 * Provides logic regarding player input (for guessed code) and guess checking 
 * EE422C programming assignment #2  
 * Name: Dedow, Karl and Fernando, Rukshinie
 * UT EID: kfd235 and rf9447
 * Lab Section: 16825 (Wednesday at 10:30)
 * EE422C - Assignment #2
 * @author Karl Dedow and Rukshinie Fernando
 * @version 1.0 2014-10-02
 * */

package Assignment2;

import java.util.Scanner;

public class Player implements Colors
{
	// Instance variables associated with player
	int blackPegs;
	int whitePegs;
	String[] guesses;
	String[] pegHistory;
	StringBuilder tempPlayer;
	StringBuilder tempCode;
	Scanner scan;

	/**
	 * Constructor for the player class. Implements a guess array to store each guess, a history
	 * array to store each peg response, a temporary string for the CPU code (used in the compare
	 * method), a temporary string for the guessed code (used in the compare method), and a scanner
	 * for all inputs from the console.
	 */
	public Player()
	{
		guesses = new String[NUM_GUESSES];
		pegHistory = new String[NUM_GUESSES];
		tempPlayer = new StringBuilder();
		tempCode = new StringBuilder();
		scan = new Scanner(System.in);
	}

	/**
	 * Allows the player to make a guess to solve the CPU code. The method asks
	 * for an input from the console that represents the four color code. If a
	 * wrong color or improperly sized code input is detected and exception is
	 * thrown that handles the situation.
	 * 
	 * @param numGuess
	 *           - the integer that represents the guess number, so as to store
	 *           each guess in the correct spot in the guesses array
	 * 
	 * @throws MastermindException
	 *            - this exception is thrown if the user input is constructed in
	 *            an incorrect way (i.e. incorrect length and incorrect input
	 *            characters)
	 */
	public void makeGuess(int numGuess)
	{
		blackPegs = 0;
		whitePegs = 0;

		// Get input that represents player guess
		System.out.println("What is your next guess?");
		System.out.println("Type in the characters for your guess and press enter");
		System.out.print("Enter guess: ");
		String guess = new String(scan.next());

		// if the input asks for the history, call the history method then ask for
		// another guess
		if (guess.equalsIgnoreCase("history"))
		{
			history(numGuess);
			makeGuess(numGuess);
			
		// Get a list of valid colors if the player inputs "colors". Then ask for
	   // another guess.
		} else if(guess.equalsIgnoreCase("colors"))
		{
			getColors();
			makeGuess(numGuess);
			
		// An input of "quit" will exit the game.
		} else if(guess.equalsIgnoreCase("quit"))
		{
			System.out.println("Goodbye! Please play again soon.");
			System.exit(-1);
		} else
		{
			// store the inputted guess in the user array (so that we can have a
			// history of guesses)
			guesses[numGuess] = guess;
			try
			{
				// make sure the input is constructed correctly, throw an exception if not
				testInput(guesses[numGuess].toString());
			} catch (MastermindException mess)
			{
				// Catch the exception then call makeGuess method again
				System.out.println("-> " + mess.getMessage());
				makeGuess(numGuess);
			}
		}
	}

	/**
	 * Checks the player guess (code) with the CPU code. This method will
	 * eventually generate the correct number of black and white pegs (which
	 * are stored as instance variables) to return to the user for feedback.
	 * 
	 * @param cpuCode
	 *           - the String that represents the computer generated code
	 * @param numGuess
	 *           , the integer that represents the guess number (used to store in
	 *           guesses array)
	 * 
	 */
	public void checkGuess(String cpuCode, int numGuess)
	{
		tempCode = new StringBuilder(cpuCode);
		tempPlayer = new StringBuilder(guesses[numGuess]);

		// Calls method that checks for black pegs.
		blackPeg();

		// Calls method that checks for white pegs
		whitePeg();
	}

	/**
	 * 
	 * Checks for black pegs. Black pegs are returned whenever there is an exact
	 * match between the player guess and the CPU code with regards to both color
	 * and location. To do this, each character in the player and CPU codes
	 * strings are compared at their corresponding indexes. If there is a
	 * character match, the blackPegs instance variable is increased by one, and
	 * the matching characters in the temporary strings representing the player
	 * and CPU codes are replaced with different characters. This guarantees
	 * matches are not duplicated (which would lead to an incorrect feedback to
	 * the player).
	 * 
	 */
	private void blackPeg()
	{
		// Compares each character in the temporary CPU and player strings
		for (int i = 0; i < NUM_SPACES; i++)
		{
			// Checks for character matches within each string
			if (tempCode.charAt(i) == tempPlayer.charAt(i))
			{
				blackPegs++;
				tempCode.replace(i, i + 1, "-");
				tempPlayer.replace(i, i + 1, "_");
			}
		}
	}

	/**
	 * 
	 * Checks for white pegs. White pegs are returned whenever there is a match
	 * in colors between the player guess and the CPU code. Also, they cannot be
	 * in matching locations. To do this, each character in the CPU code is
	 * compared to all the characters in the player code. Since the black pegs
	 * have already recorded and removed there will be no exact matches. If a
	 * match between colors (but not a match in location) is found, the whitePegs
	 * instance variable will be increased by one, and the relevant characters in
	 * the temporary strings representing the player and CPU codes are replaced
	 * with different characters. This guarantees matches are not duplicated
	 * (which would lead to an incorrect feedback to the player). Also, if a
	 * color match is found, the method will immediately increase the index for
	 * the CPU code and reset the index for the player code. This ensures that
	 * one matching color in the CPU code will not ever result in more than one
	 * white peg recorded (e.g. CPU: RYGG and Player: YRRG will not result in two
	 * white pegs when comparing R).
	 * 
	 */
	private void whitePeg()
	{
		// outer loop for the CPU code's index
		for (int i = 0; i < NUM_SPACES; i++)
		{
			// inner loop for the player's guess index
			for (int j = 0; j < NUM_SPACES; j++)
			{
				// Compare index's to see if there is a match, if so increase
				// whitePegs and break out of loop
				if (tempCode.charAt(i) == tempPlayer.charAt(j))
				{
					whitePegs++;
					tempCode.replace(i, i + 1, "-");
					tempPlayer.replace(j, j + 1, "_");
					break;
				}
			}
		}
	}

	/**
	 * Constructs the pegHistory array, which is the array that saves the
	 * feedback for each guess (i.e. the amount of black pegs and white pegs the
	 * user's guess returned). The method functions by converting the blackPegs
	 * and whitePegs instance variables into strings, then creating a new string
	 * that represents the result.
	 * 
	 * @param numGuess
	 *           - the integer that represents the guess numbers (so that the
	 *           result will be stored in the correct index)
	 */
	public void pegString(int numGuess)
	{
		// Output no pegs if no white or black pegs are recorded
		if(blackPegs == 0 && whitePegs == 0)
		{
			pegHistory[numGuess] = "Result: No pegs";
			System.out.print(guesses[numGuess] + " -> ");
			System.out.println(pegHistory[numGuess] + "\n");
		}
		
		// Output the correct amount of black and white pegs
		else
		{
			String black = Integer.toString(blackPegs);
			String white = Integer.toString(whitePegs);
			pegHistory[numGuess] = "Result: " + black + " black peg(s), " + white
				+ " white peg(s)";
			System.out.print(guesses[numGuess] + " -> ");
			System.out.println(pegHistory[numGuess] + "\n");
		}
	}

	/**
	 * Checks to see if user guess is equal to the CPU code. If so, the user has
	 * won the game and the the method will return true. If not, the user has not
	 * yet found the correct code and the method will return false
	 * 
	 * @param code
	 *           - the computer generated code. In other words, the correct code
	 *           
	 * @return the Boolean object that tells us if the user has won the game
	 *         (true means the user won, false means the guessed code is not
	 *         equal to the CPU code yet).
	 */
	public boolean winCheck(String code)
	{
		// If there are four black pegs then the user has all the colors in the
		// right spot
		if (blackPegs == 4)
		{
			// The user won!
			System.out.println("You Win!!!");
			System.out.println("Computer generated code: " + code +"\n");
			return true;
		} else
		{
			return false;
		}
	}

	/**
	 * Gives the history of guesses and their results. Since each guess stores
	 * the value of the guess (i.e. guesses) in an array, and the results of each
	 * guess in an array (i.e. pegHistory), a multi-dimensional array is
	 * constructed that relies on the guesses and results array. This newly
	 * constructed multi-dimensional array is then printed to the console.
	 * 
	 * @param numGuess
	 *           - the integer that represents how many guesses the player has
	 *           already inputted
	 */
	private void history(int numGuess)
	{
		System.out.println("History of Guesses");
		
		// Create the multi-dimesnional array
		Object[][] table = new String[numGuess + 1][3];
		table[0] = new String[]
		{ "Guess Number", " Guess", "Result" };
		
		// Fill in the multi-dimensional array
		for (int i = 1; i <= numGuess; i++)
		{

			table[i] = new String[]
			{ String.valueOf(i), guesses[i - 1],
					"                   " + pegHistory[i - 1] };
		}
		for (Object[] row : table)
		{
			System.out.format("%25s%25s%25s\n", row);
		}
	}
	
	/**
	 * Builds a string that represents all available colors that the player has available (from which
	 * to formulate a guess).The string is built in response to a player requesting valid colors.
	 * The response is printed to the console.
	 */
	private void getColors()
	{
		StringBuilder tempColor = new StringBuilder("Available colors: ");
		
		// Loop through the array containing all the names of available colors
		for (int j = 0; j < NUM_COLORS; j++)
		{
			tempColor.append(COLOR_NAMES[j]);
			
			// Make sure there are no trailing commas
			if(j != NUM_COLORS - 1){ tempColor.append(", "); }
		}
		System.out.println(tempColor + "\n");
	}

	/**
	 * Tests each user guess to make sure the guess only contains the right
	 * colors and is the right length. Does this by using the COLOR array that is
	 * in the color interface and making sure that only colors in the colors
	 * array are in the inputted player guess. Also, uses NUM_SPACES in the
	 * colors interface to make sure the inputted string is the correct length
	 * 
	 * @param userGuess
	 *           - the String that is equal the player inputted guess
	 * 
	 * @throws MastermindException
	 *            - if the guessed code is invalid, this exception is thrown
	 */
	private void testInput(String userGuess) throws MastermindException
	{
		// Check to see if the guess is the correct length
		if (userGuess.length() != NUM_SPACES)
		{
			throw new MastermindException("INVALID GUESS\n");
		}
		// check to see if the guess contains only the right colors
		else
		{
			// outer loop checks each character in the user guess
			for (int i = 0; i < NUM_SPACES; i++)
			{
				// inner loop checks each color in the COLORS array so as to
				// guarantee
				// each character in the guess is a valid color
				for (int j = 0; j < NUM_COLORS; j++)
				{
					if (userGuess.charAt(i) == COLORS[j])
					{
						// Matches a color, we are golden
						break;
					} else if (j == NUM_COLORS - 1)
					{
						// the character in the guess is not a valid color
						throw new MastermindException("INVALID GUESS\n");
					}
				}
			}
		}
	}
}
