/**
 * The colors interface contains all the valid colors. Also, contains the amount of
 * guesses allowed, the total amount of colors, and the correct size of the code (for
 * both the CPU code and the player guess)   
 * EE422C programming assignment #2  
 * Name: Dedow, Karl and Fernando, Rukshinie
 * UT EID: kfd235 and rf9447
 * Lab Section: 16825 (Wednesday at 10:30)
 * EE422C - Assignment #2
 * @author Karl Dedow and Rukshinie Fernando
 * @version 1.0 2014-10-02
 * */
package Assignment2;

public interface Colors
{
	int NUM_GUESSES = 12;
	int NUM_COLORS = 6;
	int NUM_SPACES = 4;
	char[] COLORS =
	{ 'B', 'R', 'G', 'O', 'P', 'Y' };	
	String[] COLOR_NAMES = { "Blue", "Red", "Green", "Orange", "Purple", "Yellow" };
}
