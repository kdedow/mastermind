/**
 * An exception implementation that is related to the program.
 * All the exception does is allow a message to be printed to the console.
 * EE422C programming assignment #2  
 * Name: Dedow, Karl and Fernando, Rukshinie
 * UT EID: kfd235 and rf9447
 * Lab Section: 16825 (Wednesday at 10:30)
 * EE422C - Assignment #2
 * @author Karl Dedow and Rukshinie Fernando
 * @version 1.0 2014-10-02
 * */
package Assignment2;

public class MastermindException extends Exception
{
	/**
	 * Implement the exception. Store the specific message associated with the exception.
	 */
	private static final long serialVersionUID = 720414848058257732L;
	private String message;

	public MastermindException()
	{
		super();
	}

	public MastermindException(String mess)
	{
		super(mess);
		this.message = mess;
	}

	public MastermindException(Throwable cause)
	{
		super(cause);
	}

	@Override
	public String getMessage()
	{
		return this.message;
	}
}
